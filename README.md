# Bot para mandar la previsión meteorologica diaria de un municipio con datos de Aemet

![captura del bot](capturas/aemet_bot.png)

**Características**

- Extrae datos por franjas horarias de un municipio para el día siguiente
- Formatea la salida de los datos para que sean comprensibles
- Manda los datos a un grupo o canal de telegram 

## Requerimientos

    urllib3
    requests
    
## Instrucciones

- Alojar el script weather_aemet_bot.py en un servidor, VPS o una Raspberrypi
- Crear un bot y configurarlo con @BotFather
- Añadir el bot al grupo o canal en el que se quiere recibir la previsión
- Añadir el token del bot en el apartado "TOKEN =" de weather_aemet_bot.py
- Añadir la ID del grupo o canal en el apartado "CHATID =" de weather_aemet_bot.py
- Crear un "cronjob" para que el bot publique la previsión cuando queramos. por ejemplo para que sea cada dia a las 21H: 

     0 21 * * * /usr/bin/python3 /home/pi/weather_aemet_bot.py > /tmp/listener.log 2>&1
