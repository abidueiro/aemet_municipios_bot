#!/usr/bin/env python3

import requests
import urllib3
import datetime


# Deshabilitar avisos SSL. A fecha 7-5-2020 el certificado SSL de Aemet está mal configurado y es vulnerable
# https://www.ssllabs.com/analyze.html?d=opendata.aemet.es&latest

requests.packages.urllib3.disable_warnings()
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'

try:
    requests.packages.urllib3.contrib.pyopenssl.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'
except AttributeError:
    # no pyopenssl support used / needed / available
    pass


# Mantener el enlace de donde se extraen los datos activo haciendo una llamada a la API de AEMET

http = urllib3.PoolManager()

# Adquirir una clave API desde https://opendata.aemet.es/centrodedescargas/inicio y añadir la "clave_api" detrás de ?api_key=
url = 'https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/22054/?api_key='

resp = http.request('GET', url)
print(resp.status)

# Bot telegram

def telegram_bot_sendtext(bot_message):

    TOKEN = ''
    CHATID = ''
    send_text = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?chat_id=' + CHATID + '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

    return response.json()

# Extrayendo data diaria municipio
# sustituir la URL por la que nos proporcione Aemet desde  https://opendata.aemet.es/dist/index.html?

url = 'https://opendata.aemet.es/opendata/sh/XXXXX'

resp = requests.get(url)
data = resp.json()
for elem in data:
    # print(type(elem))
    if type(elem) == dict:
        municipio = elem['nombre']
        municipio = ("🎯{}".format(municipio))
        fecha = elem['elaborado']
        fecha = ("🗓Elaborado:{}".format(fecha))
        prevision = datetime.date.today() + datetime.timedelta(days=1)
        prevision = ("🗓Previsión para:{}".format(prevision))
        Tmaxima = elem['prediccion']['dia'][1]['temperatura']['maxima']
        Tminima = elem['prediccion']['dia'][1]['temperatura']['minima']
        temperatura = ("🌡Temperatura:{}ºC/{}ºC".format(Tminima, Tmaxima))
        Smaxima = elem['prediccion']['dia'][1]['sensTermica']['maxima']
        Sminima = elem['prediccion']['dia'][1]['sensTermica']['minima']
        Sterminca =  ("🌡Sensación termica:{}ºC/{}ºC".format(Sminima, Smaxima))
        Hmaxima = elem['prediccion']['dia'][1]['humedadRelativa']['maxima']
        Hminima = elem['prediccion']['dia'][1]['humedadRelativa']['minima']
        humedad = ("💦Humedad:{}-{}%".format(Hminima, Hmaxima))
        UV = elem['prediccion']['dia'][1]['uvMax']
        uv = ("💥UV:{}".format(UV))
        periodo = elem['prediccion']['dia'][1]['probPrecipitacion'][3]['periodo']
        # 00-06
        periodo00 = elem['prediccion']['dia'][1]['probPrecipitacion'][3]['periodo']
        periodo00 = ("🕔Periodo:{}h".format(periodo00))
        cielo00 = elem['prediccion']['dia'][1]['estadoCielo'][3]['descripcion']
        if len(cielo00) == 0:
            cielo00 = ("🌍Cielo:Despejado")
        else:
            cielo00 = ("🌍Cielo:{}".format(cielo00))
        temp00 = elem['prediccion']['dia'][1]['temperatura']['dato'][0]['value']
        temp00 = ("🌡Temperatura:{}ºC".format(temp00))
        humedad00 = elem['prediccion']['dia'][1]['humedadRelativa']['dato'][0]['value']
        humedad00 = ("💦Humedad:{}%".format(humedad00))
        prob00 = elem['prediccion']['dia'][1]['probPrecipitacion'][3]['value']
        prob00 = ("☔️Probabilidad precipitación:{}%".format(prob00))
        viento00 = elem['prediccion']['dia'][1]['viento'][3]['velocidad']
        velocidad00 = elem['prediccion']['dia'][1]['viento'][3]['direccion']
        if not viento00:
            viento00 = ("💨Viento: - ")
        else:
            viento00 = ("💨Viento:{}-{}".format(viento00, velocidad00))
        nieve00 = elem['prediccion']['dia'][1]['cotaNieveProv'][3]['value']
        if not nieve00:
            nieve00 = ("☃️Cota nieve: - ")
        else:
            nieve00 = ("☃️Cota nieve:{}mts".format(nieve00))

        # 06-12
        periodo06 = elem['prediccion']['dia'][1]['probPrecipitacion'][4]['periodo']
        periodo06 = ("🕔Periodo:{}h".format(periodo06))
        cielo06 = elem['prediccion']['dia'][1]['estadoCielo'][4]['descripcion']
        if len(cielo00) == 0:
            cielo06 = ("🌍Cielo:Despejado")
        else:
            cielo06 = ("🌍Cielo:{}".format(cielo06))
        temp06 = elem['prediccion']['dia'][1]['temperatura']['dato'][1]['value']
        temp06 = ("🌡Temperatura:{}ºC".format(temp06))
        humedad06 = elem['prediccion']['dia'][1]['humedadRelativa']['dato'][1]['value']
        humedad06 = ("💦Humedad:{}%".format(humedad06))
        prob06 = elem['prediccion']['dia'][1]['probPrecipitacion'][4]['value']
        prob06 = ("☔️Probabilidad precipitación:{}%".format(prob06))
        viento06 = elem['prediccion']['dia'][1]['viento'][4]['velocidad']
        velocidad06 = elem['prediccion']['dia'][1]['viento'][4]['direccion']
        if not viento06:
            viento06 = ("💨Viento: - ")
        else:
            viento06 = ("💨Viento:{}-{}".format(viento06, velocidad06))
        nieve06 = elem['prediccion']['dia'][1]['cotaNieveProv'][4]['value']
        if not nieve06:
            nieve06 = ("☃️Cota nieve: - ")
        else:
            nieve06 = ("☃️Cota nieve:{}mts".format(nieve06))

        # 12-18
        periodo12 = elem['prediccion']['dia'][1]['probPrecipitacion'][5]['periodo']
        periodo12 = ("🕔Periodo:{}h".format(periodo12))
        cielo12 = elem['prediccion']['dia'][1]['estadoCielo'][5]['descripcion']
        if len(cielo12) == 0:
            cielo12 = ("🌍Cielo:Despejado")
        else:
            cielo12 = ("🌍Cielo:{}".format(cielo12))
        temp12 = elem['prediccion']['dia'][1]['temperatura']['dato'][2]['value']
        temp12 = ("🌡Temperatura:{}ºC".format(temp12))
        humedad12 = elem['prediccion']['dia'][1]['humedadRelativa']['dato'][2]['value']
        humedad12 = ("💦Humedad:{}%".format(humedad12))
        prob12 = elem['prediccion']['dia'][1]['probPrecipitacion'][5]['value']
        prob12 = ("☔️Probabilidad precipitación:{}%".format(prob12))
        viento12 = elem['prediccion']['dia'][1]['viento'][5]['velocidad']
        velocidad12 = elem['prediccion']['dia'][1]['viento'][5]['direccion']
        if not viento12:
            viento12 = ("💨Viento: - ")
        else:
            viento12 = ("💨Viento:{}-{}".format(viento12, velocidad12))
        nieve12 = elem['prediccion']['dia'][1]['cotaNieveProv'][5]['value']
        if not nieve12:
            nieve12 = ("☃️Cota nieve: - ")
        else:
            nieve12 = ("☃️Cota nieve:{}mts".format(nieve12))

        # 18-24
        periodo18 = elem['prediccion']['dia'][1]['probPrecipitacion'][6]['periodo']
        periodo18 = ("🕔Periodo:{}h".format(periodo18))
        cielo18 = elem['prediccion']['dia'][1]['estadoCielo'][6]['descripcion']
        if len(cielo18) == 0:
            cielo18 = ("🌍Cielo:Despejado")
        else:
            cielo18 = ("🌍Cielo:{}".format(cielo18))
        temp18 = elem['prediccion']['dia'][1]['temperatura']['dato'][3]['value']
        temp18 = ("🌡Temperatura:{}ºC".format(temp18))
        humedad18 = elem['prediccion']['dia'][1]['humedadRelativa']['dato'][3]['value']
        humedad18 = ("💦Humedad:{}%".format(humedad18))
        prob18 = elem['prediccion']['dia'][1]['probPrecipitacion'][6]['value']
        prob18 = ("☔️Probabilidad precipitación:{}%".format(prob18))
        viento18 = elem['prediccion']['dia'][1]['viento'][6]['velocidad']
        velocidad18 = elem['prediccion']['dia'][1]['viento'][6]['direccion']
        if not viento18:
            viento18 = ("💨Viento: - ")
        else:
            viento18 = ("💨Viento:{}-{}".format(viento18, velocidad18))
        nieve18 = elem['prediccion']['dia'][1]['cotaNieveProv'][6]['value']
        if not nieve18:
            nieve18 = ("☃️Cota nieve: - ")
        else:
            nieve18 = ("☃️Cota nieve:{}mts".format(nieve18))


        benasque = municipio + "\n" + \
                   fecha + "\n\n" + \
                   prevision + "\n" + \
                   temperatura + "\n" + \
                   Sterminca + "\n" + \
                   humedad + "\n" + \
                   uv + "\n\n" + \
                   periodo00 + "\n" + \
                   cielo00 + "\n" + \
                   temp00 + "\n" + \
                   humedad00 + "\n" + \
                   prob00 + "\n" + \
                   viento00 + "\n" + \
                   nieve00 + "\n\n" + \
                   periodo06 + "\n" + \
                   cielo06 + "\n" + \
                   temp06 + "\n" + \
                   humedad06 + "\n" + \
                   prob06 + "\n" + \
                   viento06 + "\n" + \
                   nieve06 + "\n\n" + \
                   periodo12 + "\n" + \
                   cielo12 + "\n" + \
                   temp12 + "\n" + \
                   humedad12 + "\n" + \
                   prob12 + "\n" + \
                   viento12 + "\n" + \
                   nieve12 + "\n\n" + \
                   periodo18 + "\n" + \
                   cielo18 + "\n" + \
                   temp18 + "\n" + \
                   humedad18 + "\n" + \
                   prob18 + "\n" + \
                   viento18 + "\n" + \
                   nieve18

        telegram_bot_sendtext(benasque)
